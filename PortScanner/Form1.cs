using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;

namespace PortScanner
{
    public partial class frmMain : Form
    {
                   

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            txtLog.Clear();
            List<int> ports = new List<int>(); 
            ports.Add(8002);                  
            ports.Add(8003);                   
            ports.Add(8005);                
            ports.Add(8007);
            ports.Add(9000);
            ports.Add(9001); 

            if (txtIP.Text == "")
            {
                MessageBox.Show("Please enter the IP or DNS name of the server");
            }
            else
            {
                // Reset the progress bar
                prgScanning.Value = 0;
                // Set the max value of the progress bar
                prgScanning.Maximum = 6 ;
                // Let the user know the application is busy
                Cursor.Current = Cursors.WaitCursor;
                // Loop through the ports between start port and end port
                foreach (int port in ports)
                {

                    TcpClient TcpScan = new TcpClient();
                    try
                    {
                        // Try to connect
                        TcpScan.Connect(txtIP.Text, port);
                        // If there's no exception, we can say the port is open
                        txtLog.AppendText("Port " + port + " open\r\n");
                    }
                    catch
                    {
                        // An exception occured, thus the port is probably closed
                        txtLog.AppendText("Port " + port + " closed\r\n");
                    }
                    // Increase the progress on the progress bar
                    prgScanning.PerformStep();
                }
                // Set the cursor back to normal
                Cursor.Current = Cursors.Arrow;
            }
        }

               
    }
}